CREATE TABLE IF NOT EXISTS "events" (
    "event_id" serial PRIMARY KEY,
    "name" VARCHAR NOT NULL,
    "start" TIMESTAMP NOT NULL,
    "end" TIMESTAMP NOT NULL,
    "type" INT NOT NULL,
    "notified" BOOLEAN DEFAULT FALSE
);
