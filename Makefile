.PHONY: build

build:
	go build -o build/calendar cmd/calendar/*.go
	go build -o build/scheduler cmd/scheduler/*.go
	go build -o build/sender cmd/sender/*.go

run:
	docker-compose -f deployments/docker-compose.yml up -d

rebuild:
	docker-compose -f deployments/docker-compose.yml build

stop:
	docker-compose -f deployments/docker-compose.yml down

restart: stop run

test:
	set -e ;\
	docker-compose -f deployments/docker-compose.test.yml up --build -d ;\
	docker-compose -f deployments/docker-compose.test.yml run integration-tests go test -v -mod=vendor ./tests/... ;\
	test_status_code=$$? ;\
	docker-compose -f deployments/docker-compose.test.yml down ;\
	exit $$test_status_code ;\

make stop-test:
	docker-compose -f deployments/docker-compose.test.yml down

lint:
	golangci-lint run ./...

protos:
	protoc -I/usr/local/include -I. \
		-I$$GOPATH/src \
		-I$$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--go_out=plugins=grpc:./pkg/calendar \
		./api/proto/calendar.proto

	protoc -I/usr/local/include -I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--grpc-gateway_out=logtostderr=true:./pkg/calendar \
		--swagger_out=logtostderr=true:. \
		./api/proto/calendar.proto
