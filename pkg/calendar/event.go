package calendar

import "time"

// EventType - enum for event types
type EventType int32

const (
	// Other - particular event type not defined
	Other EventType = iota
	// Meeting event type
	Meeting
	// Reminder event type
	Reminder
)

// Event ...
type Event struct {
	ID    uint64
	Name  string     `json:"name"`
	Start *time.Time `json:"start"`
	End   *time.Time `json:"end"`
	Type  EventType  `json:"type"`
}
