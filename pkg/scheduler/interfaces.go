package scheduler

import (
	"context"
)

type Repository interface {
	GetNotifications(context.Context) ([]Notification, error)
	Close() error
}

type Queue interface {
	EnqueueNotification(Notification) error
	Close() error
}
