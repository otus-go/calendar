package scheduler

type Notification struct {
	Address string
	Content string
}
