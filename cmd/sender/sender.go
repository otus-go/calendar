// entry point to application

package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/prometheus/client_golang/prometheus/promhttp"

	"calendar/pkg/sender"

	"github.com/jpillora/backoff"
	"github.com/kelseyhightower/envconfig"
	"github.com/prometheus/client_golang/prometheus"

	implementation "calendar/internal/sender"
)

type Config struct {
	Queue string `default:"amqp://guest:guest@localhost:5672/"`
}

var (
	counter = prometheus.NewCounter(
		prometheus.CounterOpts{
			Name: "sender_notifications_total",
			Help: "Sent notifications counter",
		},
	)
)

func init() {
	prometheus.MustRegister(counter)
}

func main() {
	go runPrometheusServer()

	config := &Config{}
	_ = envconfig.Process("sender", config)

	b := &backoff.Backoff{} // exponential backoff helper

	// use signal handler and context with cancel for graceful shutdown
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)

	ctx, cancel := context.WithCancel(context.Background())
	go handleSignals(signals, cancel)

	log.Println("starting service...")
	defer log.Println("service stoppped")

	// infinite loop. creates/recreates service if context not cancelled
	for {
		select {
		case <-ctx.Done():
			return
		default:
			queue, err := implementation.NewAMQPQueue(config.Queue)
			if err != nil {
				d := b.Duration()
				log.Printf("%s, retry after %v\n", err, d)
				time.Sleep(d)

				continue
			}

			b.Reset() // reset backoff if queue initialized successfully

			service := sender.Service{
				Metrics: &implementation.PrometheusMetrics{Counter: counter},
				Queue:   queue,
				Sender:  &implementation.StdoutSender{},
			}

			err = service.Serve(ctx)
			if err != nil {
				log.Println(err)
			}
		}
	}
}

// handleSignal - cancel context on signal
func handleSignals(signals chan os.Signal, cancel context.CancelFunc) {
	<-signals
	cancel()
	log.Println("stopping service...")
}

func runPrometheusServer() {
	log.Println("run prometheus exporter server")
	http.Handle("/metrics", promhttp.Handler())
	_ = http.ListenAndServe(":9187", nil)
}
