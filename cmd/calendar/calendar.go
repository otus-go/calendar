package main

import (
	"context"
	"log"
	"net"
	"net/http"
	"time"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	_ "github.com/jackc/pgx/stdlib"
	"github.com/kelseyhightower/envconfig"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"calendar/internal/calendar/repositories"
	"calendar/pkg/calendar"
	pb "calendar/pkg/calendar/api/proto"
)

var (
	grpcRequests = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "calendar_grpc_requests_count",
		Help: "Total number of RPCs handled on the server.",
	}, []string{"method", "code"})

	latencies = prometheus.NewSummaryVec(prometheus.SummaryOpts{
		Name:       "calendar_grpc_request_latency_seconds",
		Help:       "The temperature of the frog pond.",
		Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.97: 0.001, 0.99: 0.001},
	}, []string{"method", "code"})
)

// Config contains databasse connection string and other options
type Config struct {
	Database string `default:"postgres://postgres@localhost/calendar?sslmode=disable"`
}

func metricsInterceptor(
	ctx context.Context,
	request interface{},
	info *grpc.UnaryServerInfo,
	handler grpc.UnaryHandler,
) (interface{}, error) {
	handlingBeginsAt := time.Now()
	code := codes.OK

	res, err := handler(ctx, request)
	if err != nil {
		if st, ok := status.FromError(err); ok {
			code = st.Code()
		} else {
			code = codes.Unknown
		}
	}

	labels := prometheus.Labels{
		"method": info.FullMethod,
		"code":   code.String(),
	}
	latencies.With(labels).Observe(time.Since(handlingBeginsAt).Seconds())
	grpcRequests.With(labels).Inc()

	return res, err
}

func init() {
	prometheus.MustRegister(grpcRequests)
	prometheus.MustRegister(latencies)
}

func main() {
	go runPrometheusServer()

	// init and read config from environment
	config := &Config{}
	_ = envconfig.Process("calendar", config) // deliberately ignore error

	// start gRPC
	go func() {
		lis, err := net.Listen("tcp", "localhost:28080")
		if err != nil {
			log.Fatal("Cannot open socket")
		}

		repository, err := repositories.NewPostgresRepository(
			context.Background(), config.Database)
		if err != nil {
			log.Fatal("Cannot create repository: ", err)
		}
		defer repository.Close()
		calendarInstance := calendar.NewCalendar(repository)
		calendarService := pb.Calendar{Calendar: calendarInstance}

		grpcServer := grpc.NewServer(grpc.UnaryInterceptor(metricsInterceptor))
		pb.RegisterCalendarServiceServer(grpcServer, calendarService)
		log.Println("Starting api server")
		_ = grpcServer.Serve(lis)
	}()

	ctx := context.Background()

	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}
	err := pb.RegisterCalendarServiceHandlerFromEndpoint(ctx, mux, ":28080", opts)

	if err != nil {
		log.Fatal("Cannot register handler")
	}

	log.Println("Starting HTTP server")
	log.Fatalln(http.ListenAndServe(":8080", mux))
}

func runPrometheusServer() {
	log.Println("run prometheus exporter server")
	http.Handle("/metrics", promhttp.Handler())
	_ = http.ListenAndServe(":9187", nil)
}
