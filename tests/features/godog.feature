Feature: create notification
  If your memory doesn't serve you anymore
  Use notepad and better two notepads, as me

  Scenario: Create event
    Given calendar service running on host "calendar" and port 8080
    When I send "POST" request to create event endpoint "/v1/events"
    Then there should be code 200 response
    And Receive notification


  Scenario: Read event
    When I sent "GET" request to endpoint "/v1/events/1"
    Then there should be code 200 response

  Scenario: Delete event
    When I sent "DELETE" request to endpoint "/v1/events/1"
    Then there should be code 200 response

  Scenario: Read event
    When I sent "GET" request to endpoint "/v1/events/1"
    Then there should be code 404 response
