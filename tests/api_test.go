package tests

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"os"
	"time"

	"github.com/DATA-DOG/godog"
	"github.com/DATA-DOG/godog/gherkin"
	"github.com/streadway/amqp"
)

type apiTest struct {
	resp        *http.Response
	address     string
	qConnection *amqp.Connection
	qChannel    *amqp.Channel
	qDelivery   <-chan amqp.Delivery
}

// Create

func (test *apiTest) calendarServiceRunningOnHostAndPort(host string, port int) error {
	test.address = fmt.Sprintf("%s:%d", host, port)

	conn, err := net.Dial("tcp", test.address)
	if err != nil {
		return fmt.Errorf("unable to connect to service: %w", err)
	}

	defer conn.Close()

	return nil
}

func (test *apiTest) iSendRequestToCreateEventEndpoint(method string, endpoint string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	body, err := json.Marshal(map[string]string{
		"name":  "Event 1",
		"start": time.Now().Add(3 * time.Minute).Format(time.RFC3339),
		"end":   time.Now().Add(10 * time.Minute).Format(time.RFC3339),
	})
	if err != nil {
		return err
	}

	buf := bytes.NewBuffer(body)

	req, err := http.NewRequestWithContext(
		ctx,
		method,
		fmt.Sprintf("http://%s%s", test.address, endpoint),
		buf,
	)
	if err != nil {
		return err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}

	test.resp = resp

	return resp.Body.Close()
}

func (test *apiTest) receiveNotification() error {
	select {
	case <-time.After(10 * time.Second):
		return fmt.Errorf("message is not received")
	case event := <-test.qDelivery:
		fmt.Println("Event received: ", string(event.Body))
		return nil
	}
}

// read

func (test *apiTest) iSentRequestToEndpoint(method, endpoint string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	req, err := http.NewRequestWithContext(
		ctx, method,
		fmt.Sprintf("http://%s%s", test.address, endpoint),
		nil,
	)
	if err != nil {
		return err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}

	test.resp = resp

	return resp.Body.Close()
}

func (test *apiTest) thereShouldBeCodeResponse(status int) error {
	if test.resp.StatusCode != status {
		return fmt.Errorf("want StatusCode %d, got %d", status, test.resp.StatusCode)
	}

	return nil
}

func FeatureContext(s *godog.Suite) {
	c := &apiTest{}
	s.BeforeFeature(c.featureSetUp)
	// create
	s.Step(`^calendar service running on host "([^"]*)" and port (\d+)$`, c.calendarServiceRunningOnHostAndPort)
	s.Step(`^I send "([^"]*)" request to create event endpoint "([^"]*)"$`, c.iSendRequestToCreateEventEndpoint)
	s.Step(`^there should be code (\d+) in response$`, c.thereShouldBeCodeResponse)
	s.Step(`^Receive notification$`, c.receiveNotification)
	// read
	s.Step(`^I sent "([^"]*)" request to endpoint "([^"]*)"$`, c.iSentRequestToEndpoint)
	s.Step(`^there should be code (\d+) response$`, c.thereShouldBeCodeResponse)
	// delete
	s.Step(`^I sent "([^"]*)" request to endpoint "([^"]*)"$`, c.iSentRequestToEndpoint)

	s.AfterFeature(c.featureTearDown)
}

func (test *apiTest) featureSetUp(*gherkin.Feature) {
	fmt.Println("Waiting for rabbit availability")

	amqpDSN := os.Getenv("RABBIT_DSN")
	if amqpDSN == "" {
		amqpDSN = "amqp://guest:guest@rabbit:5672/"
	}

	var err error

	attempts := 10
	for i := 0; i < attempts; i++ {
		test.qConnection, err = amqp.Dial(amqpDSN)
		if err == nil {
			break
		}

		time.Sleep(1 * time.Second)
	}
	panicOnErr(err)

	test.qChannel, err = test.qConnection.Channel()
	panicOnErr(err)

	err = test.qChannel.ExchangeDeclare("notifications", "fanout", true, false, false, false, nil)
	panicOnErr(err)

	// Consume
	q, err := test.qChannel.QueueDeclare("", false, false, true, false, nil)
	panicOnErr(err)

	err = test.qChannel.QueueBind(q.Name, "", "notifications", false, nil)
	panicOnErr(err)

	events, err := test.qChannel.Consume(q.Name, "", true, false, false, false, nil)
	panicOnErr(err)

	test.qDelivery = events
}

func (test *apiTest) featureTearDown(*gherkin.Feature) {
	panicOnErr(test.qChannel.Close())
	panicOnErr(test.qConnection.Close())
}

func panicOnErr(err error) {
	if err != nil {
		panic(err)
	}
}
