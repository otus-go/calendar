package sender

import "github.com/prometheus/client_golang/prometheus"

type PrometheusMetrics struct {
	Counter prometheus.Counter
}

func (c *PrometheusMetrics) IncrementNotificationsCounter() {
	c.Counter.Inc()
}
