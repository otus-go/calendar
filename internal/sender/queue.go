package sender

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"

	"calendar/pkg/sender"

	"github.com/streadway/amqp"
)

// rabbit MQ implementation
type AMQPQueue struct {
	conn *amqp.Connection
	ch   *amqp.Channel
	q    amqp.Queue
	msgs <-chan amqp.Delivery
}

func (q *AMQPQueue) declareQueue() (amqp.Queue, error) {
	return q.ch.QueueDeclare(
		"notifications", // name
		true,            // durable
		false,           // delete when usused
		false,           // exclusive
		false,           // no-wait
		nil,             // arguments
	)
}

func (q *AMQPQueue) setQoS() error {
	return q.ch.Qos(
		1,     // prefetch count
		0,     // prefetch size
		false, // global
	)
}

// Close resources
func (q *AMQPQueue) Close() error {
	var chErr, connErr error
	if q.ch != nil {
		chErr = q.ch.Close()
	}

	if q.conn != nil {
		connErr = q.conn.Close()
	}

	if chErr != nil || connErr != nil {
		return errors.New("failed to close queue")
	}

	return nil
}

func (q *AMQPQueue) EmitEvent(notification *sender.Notification) {
	body, _ := json.Marshal(notification)
	_ = q.ch.Publish(
		"notifications",
		"",
		false,
		false,
		amqp.Publishing{
			ContentType: "application/json",
			Body:        body,
		})
}

// Messages - fetch messages from rabbitmq
func (q *AMQPQueue) Messages() <-chan sender.Message {
	ch := make(chan sender.Message, 1)

	go func() {
		defer close(ch)
		for {
			msg, ok := <-q.msgs
			var result sender.Message
			if !ok {
				result = sender.Message{
					Notification: nil,
					Error:        fmt.Errorf("%w: queue closed", sender.ErrConnectionFailed),
				}
				ch <- result
			}
			notification := &sender.Notification{}
			err := json.Unmarshal(msg.Body, notification)
			_ = msg.Ack(false)
			if err != nil {
				result = sender.Message{
					Notification: nil,
					Error:        fmt.Errorf("%w: %s", sender.ErrDecodingFailed, err),
				}
				ch <- result
				continue
			}
			result = sender.Message{
				Notification: notification,
				Error:        nil,
			}
			ch <- result
		}
	}()

	return ch
}

// NewAMQPQueue -
func NewAMQPQueue(connString string) (*AMQPQueue, error) {
	queue := &AMQPQueue{}
	// open connection
	conn, err := amqp.Dial(connString)
	if err != nil {
		return queue, fmt.Errorf("unable to connect to the queue: %w", err)
	}

	queue.conn = conn

	// create channel
	ch, err := conn.Channel()
	if err != nil {
		return queue, fmt.Errorf("unable to create queue channel: %w", err)
	}

	err = ch.ExchangeDeclare("notifications", "fanout", true, false, false, false, nil)
	if err != nil {
		log.Fatalln("unable to declare exchange: ", err)
	}

	queue.ch = ch

	// declare queue
	q, err := queue.declareQueue()
	if err != nil {
		log.Fatalln("unable to declare queue: ", err)
	}

	queue.q = q
	// ser QoS
	err = queue.setQoS()
	if err != nil {
		return queue, fmt.Errorf("unable to set a QoS: %w", err)
	}

	// start consume
	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		false,  // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	if err != nil {
		return queue, fmt.Errorf("unable to create consumer: %w", err)
	}

	queue.msgs = msgs

	log.Println("connection established")

	return queue, nil
}
