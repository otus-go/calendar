package sender

import (
	"calendar/pkg/sender"
	"context"
	"fmt"
)

type StdoutSender struct{}

func (s *StdoutSender) Send(ctx context.Context, n *sender.Notification) error {
	fmt.Printf("Send notification to <%s>: [%s]\n", n.Address, n.Content)
	return nil
}
