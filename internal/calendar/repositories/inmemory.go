// implements repository interface for in memory storage

package repositories

import (
	calendar2 "calendar/pkg/calendar"
	"context"
	"sync"
)

// InMemoryRepository - data access object for events
// temporary implementation based on simple map
type InMemoryRepository struct {
	sync.RWMutex
	Map     map[uint64]*calendar2.Event
	Counter uint64
}

// NewInMemoryRepository - constructor for data access object
func NewInMemoryRepository() *InMemoryRepository {
	return &InMemoryRepository{
		Map:     make(map[uint64]*calendar2.Event),
		Counter: 1,
	}
}

// Create - save new value into the Storage
func (r *InMemoryRepository) Create(ctx context.Context, e *calendar2.Event) (uint64, error) {
	// Lock and save new value
	r.Lock()
	e.ID = r.Counter
	r.Map[r.Counter] = e
	res := r.Counter
	r.Counter++
	r.Unlock()

	return res, nil
}

// Read - return value from storage
func (r *InMemoryRepository) Read(ctx context.Context, id uint64) (*calendar2.Event, error) {
	// Lock and save new value
	r.RLock()
	v, ok := r.Map[id]
	r.RUnlock()

	if ok {
		return v, nil
	}

	return nil, calendar2.ErrRecordNotFound
}

// Update - rewrite existing value in storage
func (r *InMemoryRepository) Update(ctx context.Context, id uint64, e *calendar2.Event) error {
	r.RLock()
	_, ok := r.Map[id]
	r.RUnlock()

	if !ok {
		return calendar2.ErrRecordNotFound
	}

	r.Lock()
	e.ID = id
	r.Map[id] = e
	r.Unlock()

	return nil
}

// Delete - remove value from storage
func (r *InMemoryRepository) Delete(ctx context.Context, id uint64) error {
	r.Lock()
	delete(r.Map, id)
	r.Unlock()

	return nil
}
