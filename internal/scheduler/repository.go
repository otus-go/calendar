package scheduler

import (
	"calendar/pkg/scheduler"
	"context"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
)

type DBEvent struct {
	Name string `db:"name"`
}

func (e DBEvent) ToNotification() scheduler.Notification {
	return scheduler.Notification{
		Address: "qwe@example.com", // there is no adresses or usernames in our sandbox
		Content: fmt.Sprintf("%s is coming!", e.Name),
	}
}

type DBEventsList []DBEvent

func (l DBEventsList) ToNotificationsList() []scheduler.Notification {
	n := make([]scheduler.Notification, 0, len(l))
	for _, e := range l {
		n = append(n, e.ToNotification())
	}

	return n
}

// PostgresRepository - pgx based data access object for events
type PostgresRepository struct {
	db *sqlx.DB
}

// Close - closes connection to db
func (p *PostgresRepository) Close() error {
	return p.db.Close()
}

// GetNotifications returns notifications for queueing for sending
func (p *PostgresRepository) GetNotifications(ctx context.Context) ([]scheduler.Notification, error) {
	now := time.Now().Add(5 * time.Minute)

	// use sqlx.Tx transaction instead sql.Tx
	tx, err := p.db.BeginTxx(ctx, nil)
	if err != nil {
		return nil, err
	}

	notifications, err := selectNotifications(ctx, tx, now)
	if err != nil {
		_ = tx.Rollback()
		return nil, err
	}

	err = updateNotifications(ctx, tx, now)
	if err != nil {
		_ = tx.Rollback()
		return nil, err
	}

	_ = tx.Commit()

	return notifications, nil
}

// select events from database and convert to notifications
func selectNotifications(ctx context.Context, tx *sqlx.Tx, now time.Time) ([]scheduler.Notification, error) {
	events := &DBEventsList{}
	q := `
		SELECT "name" FROM "events"
		WHERE "start" <= $1 AND "notified" = FALSE
	`
	err := tx.SelectContext(ctx, events, q, now)

	return events.ToNotificationsList(), err
}

// marks events as notified in database
func updateNotifications(ctx context.Context, tx *sqlx.Tx, now time.Time) error {
	updateQuery := `
		UPDATE "events"
		SET "notified" = TRUE
		WHERE "start" <= $1 AND "notified" = FALSE
	`
	_, err := tx.ExecContext(ctx, updateQuery, now)

	return err
}

// NewPostgresRepository - constructor for data access object
func NewPostgresRepository(ctx context.Context, dsn string) (*PostgresRepository, error) {
	// create database object
	db, err := sqlx.ConnectContext(ctx, "pgx", dsn)

	if err != nil {
		return nil, err
	}
	// check connection to db
	err = db.Ping()
	if err != nil {
		return nil, err
	}

	return &PostgresRepository{
		db: db,
	}, nil
}
