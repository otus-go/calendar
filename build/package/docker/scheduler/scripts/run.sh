#!/usr/bin/env bash

sh wait-for.sh -t 10 postgres:5432
sh wait-for.sh -t 10 rabbit:5672
/go/bin/scheduler